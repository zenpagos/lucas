package model

const (
	VisaPaymentMethod       = "visa"
	MastercardPaymentMethod = "mastercard"
	CabalPaymentMethod      = "cabal"
	NaranjaPaymentMethod    = "naranja"
	AmexPaymentMethod       = "amex"

	CreditCardPaymentMethodType = "credit.card"
	DebitCardPaymentMethodType  = "debit.card"

	CUITTINType = "cuit"
	CUILTINType = "cuil"
	DUTINType   = "du"

	ARSCurrency       = "ars"
	SinglePaymentType = "single"
	MerchantChannel   = "005"

	DevDecidirEnv  = "dev"
	ProdDecidirEnv = "prod"
)
