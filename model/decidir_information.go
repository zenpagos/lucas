package model

type DecidirInformation struct {
	DecidirPaymentID     *int64  `sql:"type:int"`
	DecidirPaymentStatus *string `sql:"type:varchar(255)"`

	DecidirPaymentMethodID int64  `sql:"type:int;not null"`
	DecidirPaymentType     string `sql:"type:varchar(255);not null"`
	DecidirEnvironment     string `sql:"type:varchar(255);not null"`
	DecidirPrivateKey      string `sql:"type:varchar(255);not null"`
	DecidirPaymentToken    string `sql:"type:varchar(255);not null"`
}
