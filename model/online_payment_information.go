package model

type OnlinePaymentInformation struct {
	PaymentMethod       string `sql:"type:varchar(255);not null"`
	PaymentMethodType   string `sql:"type:varchar(255);not null"`
	Description         string `sql:"type:varchar(255);not null"`
	StatementDescriptor string `sql:"type:varchar(255);not null"`
	Currency            string `sql:"type:varchar(255);not null"`
	Amount              int64  `sql:"type:int;not null"`
	Installments        int64  `sql:"type:int;not null"`
	CardBin             string `sql:"type:varchar(255);not null"`
}
