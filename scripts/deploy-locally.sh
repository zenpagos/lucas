#!/bin/bash

SVC_DEPLOYMENT=lucas

docker build -t 127.0.0.1:5000/lucas .

docker push 127.0.0.1:5000/lucas

helm upgrade --install \
  -f scripts/lucas-values.yaml \
  ${SVC_DEPLOYMENT} kubernetes/lucas
