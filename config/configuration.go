package config

type Configuration struct {
	Service  ServiceConfiguration  `mapstructure:"service"`
	Database DatabaseConfiguration `mapstructure:"database"`
}
