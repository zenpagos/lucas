// Package registration represents the concrete implementation of DecidirOnlinePaymentUseCase interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package deonpayuc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/lucas/decidir"
	"gitlab.com/zenpagos/lucas/model"
	"gitlab.com/zenpagos/lucas/repository"
	"gitlab.com/zenpagos/lucas/usecase"
	"gitlab.com/zenpagos/tools"
	"strings"
)

type DecidirOnlinePaymentUseCase struct {
	log                            *logrus.Logger
	DecidirClient                  *decidir.Client
	DecidirOnlinePaymentRepository repository.DecidirOnlinePaymentRepositoryInterface
}

func NewDecidirOnlinePaymentUseCase(
	log *logrus.Logger,
	dcd *decidir.Client,
	dopr repository.DecidirOnlinePaymentRepositoryInterface,
) usecase.DecidirOnlinePaymentUseCaseInterface {
	return DecidirOnlinePaymentUseCase{
		log:                            log,
		DecidirClient:                  dcd,
		DecidirOnlinePaymentRepository: dopr,
	}
}

func (uc DecidirOnlinePaymentUseCase) getDecidirPaymentMethodID(dop *model.DecidirOnlinePayment) error {
	pm := dop.PaymentMethod
	pmt := dop.GetCleanPaymentMethodType()

	dpmID, err := uc.DecidirClient.GetPaymentMethodID(pm, pmt)
	if err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	dop.DecidirPaymentMethodID = dpmID

	return nil
}

func (uc DecidirOnlinePaymentUseCase) executeDecidirPayment(dp *decidir.Payment, dop *model.DecidirOnlinePayment) error {
	dp.SiteTransactionID = dop.ID.String()
	dp.Token = dop.DecidirPaymentToken
	dp.PaymentMethodID = dop.DecidirPaymentMethodID
	dp.Bin = dop.CardBin
	dp.Amount = dop.Amount
	dp.Currency = strings.ToUpper(dop.Currency)
	dp.Installments = dop.Installments
	dp.Description = dop.Description
	dp.EstablishmentName = dop.StatementDescriptor
	dp.PaymentType = dop.DecidirPaymentType
	dp.SubPayments = []decidir.SubPayment{}
	dp.AggregateData = decidir.AggregateData{
		Indicator:            "1", // TODO take a look this
		IdentificationNumber: dop.MerchantTIN,
		BillToPay:            dop.ID.String(), // TODO take a look this
		BillToRefund:         dop.ID.String(), // TODO take a look this
		MerchantName:         dop.MerchantName,
		Street:               dop.MerchantStreet,
		Number:               dop.MerchantDoorNumber,
		PostalCode:           dop.MerchantZipCode,
		Category:             dop.MerchantCategory,
		Channel:              dop.MerchantChannel,
		GeographicCode:       dop.MerchantGeographicCode,
		City:                 dop.MerchantCity,
		MerchantID:           dop.MerchantID,
		Province:             dop.MerchantProvince,
		Country:              dop.MerchantCountry,
		MerchantEmail:        dop.MerchantEmail,
		MerchantPhone:        dop.MerchantPhone,
	}
	dp.Customer = decidir.Customer{
		ID:    dop.PayerTIN,
		Email: dop.PayerEmail,
	}

	if err := uc.DecidirClient.ExecutePayment(dp); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc DecidirOnlinePaymentUseCase) ExecutePayment(dop *model.DecidirOnlinePayment) error {
	// validate the information received for the user
	if err := dop.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	dop.DecidirPaymentType = model.SinglePaymentType
	dop.PayerInformation.PayerTINType = model.DUTINType
	dop.MerchantInformation.MerchantChannel = model.MerchantChannel

	if err := uc.getDecidirPaymentMethodID(dop); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	// insert the new order to database
	if err := uc.DecidirOnlinePaymentRepository.Insert(dop); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	// set decidir credentials and environment
	uc.DecidirClient.SetEnvironment(dop.DecidirEnvironment)
	uc.DecidirClient.SetCredentials(dop.DecidirPrivateKey)

	dp := new(decidir.Payment)
	if err := uc.executeDecidirPayment(dp, dop); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	dop.DecidirPaymentID = tools.Int64PointerOrNil(dp.ID)
	dop.DecidirPaymentStatus = dp.Status

	// log the payment object if is not approved
	if tools.StringValueOrBlank(dp.Status) != decidir.ApprovedStatus {
		uc.log.WithFields(logrus.Fields{"decidir_payment": dp}).Info()
	}

	if err := uc.DecidirOnlinePaymentRepository.Update(dop); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}
