// Package usecase defines all the interfaces for a Micro-service application.
// It is the entry point for the application's business logic. It is a top level package for a Micro-service application.
// This top level package only defines interface, the concrete implementations are defined in sub-package of it.
// It only depends on model package. No other package should dependent on it except cmd.

// If transaction is supported, the transaction boundary should be defined in this package.
// A suffix-"WithTx" can be added to the name of a transaction function to distinguish it from a non-transaction one.
package usecase

import "gitlab.com/zenpagos/lucas/model"

type DecidirOnlinePaymentUseCaseInterface interface {
	ExecutePayment(decidirOnlinePayment *model.DecidirOnlinePayment) error
}
