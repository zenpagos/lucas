package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/lucas/config"
	"gitlab.com/zenpagos/lucas/container"
	"gitlab.com/zenpagos/lucas/decidir"
	"gitlab.com/zenpagos/lucas/interface/igrpc"
	pbv1 "gitlab.com/zenpagos/lucas/proto/v1"
	"gitlab.com/zenpagos/lucas/repository/mysqlrepo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
)

var log = logrus.New()

func init() {
	log.SetReportCaller(true)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig(log)
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf, log)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}

	addr := fmt.Sprintf(":%d", conf.Service.Port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	decidirClient := decidir.NewDecidirClient(log)
	serviceContainer := container.InitializeContainer(db, log, decidirClient)

	gRPCServer := grpc.NewServer()

	lucasService := igrpc.NewGRPCServer(log, serviceContainer)
	pbv1.RegisterLucasServiceServer(gRPCServer, lucasService)

	healthService := igrpc.NewHealthChecker()
	grpc_health_v1.RegisterHealthServer(gRPCServer, healthService)

	if err := gRPCServer.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}
}
