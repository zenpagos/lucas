package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/lucas/container"
	"gitlab.com/zenpagos/lucas/model"
	"gitlab.com/zenpagos/lucas/proto/v1"
	"gitlab.com/zenpagos/tools"
)

type gRPCServer struct {
	log       *logrus.Logger
	container container.Container
}

func NewGRPCServer(log *logrus.Logger, c container.Container) pbv1.LucasServiceServer {
	return &gRPCServer{
		log:       log,
		container: c,
	}
}

func (s *gRPCServer) PayOnlineWithDecidir(
	_ context.Context,
	req *pbv1.PayOnlineWithDecidirRequest,
) (*pbv1.PayOnlineWithDecidirResponse, error) {

	op := &model.DecidirOnlinePayment{
		OnlinePaymentInformation: model.OnlinePaymentInformation{
			PaymentMethod:       req.PaymentMethod,
			PaymentMethodType:   req.PaymentMethodType,
			Description:         req.Description,
			StatementDescriptor: req.StatementDescriptor,
			Currency:            req.Currency,
			Amount:              req.Amount,
			Installments:        req.Installments,
			CardBin:             req.CardBin,
		},
		DecidirInformation: model.DecidirInformation{
			DecidirEnvironment:  req.DecidirEnvironment,
			DecidirPrivateKey:   req.DecidirPrivateKey,
			DecidirPaymentToken: req.DecidirPaymentToken,
		},
		MerchantInformation: model.MerchantInformation{
			MerchantTIN:            req.MerchantTIN,
			MerchantTINType:        req.MerchantTINType,
			MerchantName:           req.MerchantName,
			MerchantStreet:         req.MerchantStreet,
			MerchantDoorNumber:     req.MerchantDoorNumber,
			MerchantZipCode:        req.MerchantZipCode,
			MerchantCategory:       req.MerchantCategory,
			MerchantGeographicCode: req.MerchantGeographicCode,
			MerchantCity:           req.MerchantCity,
			MerchantID:             req.MerchantID,
			MerchantProvince:       req.MerchantProvince,
			MerchantCountry:        req.MerchantCountry,
			MerchantEmail:          req.MerchantEmail,
			MerchantPhone:          req.MerchantPhone,
		},
		PayerInformation: model.PayerInformation{
			PayerTIN:   req.PayerTIN,
			PayerEmail: req.PayerEmail,
		},
	}

	if err := s.container.DecidirOnlinePaymentUseCase.ExecutePayment(op); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	res := pbv1.PayOnlineWithDecidirResponse{
		ID:               op.ID.String(),
		CreatedAt:        op.CreatedAt,
		UpdatedAt:        tools.Int64ValueOrZero(op.UpdatedAt),
		Status:           tools.StringValueOrBlank(op.DecidirPaymentStatus),
		DecidirPaymentID: tools.Int64ValueOrZero(op.DecidirPaymentID),
	}

	return &res, nil
}
