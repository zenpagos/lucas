package decidir

type Customer struct {
	ID    string `json:"id"`
	Email string `json:"email"`
}

type ErrorReason struct {
	ID                    int64  `json:"id"`
	Description           string `json:"description"`
	AdditionalDescription string `json:"additional_description"`
}

type StatusDetailError struct {
	Type   string      `json:"type"`
	Reason ErrorReason `json:"reason"`
}

type StatusDetail struct {
	Ticket                string            `json:"ticket"`
	CardAuthorizationCode string            `json:"card_authorization_code"`
	AddressValidationCode string            `json:"address_validation_code"`
	Error                 StatusDetailError `json:"error"`
}

type AggregateData struct {
	Indicator            string `json:"indicator"`
	IdentificationNumber string `json:"identification_number"`
	BillToPay            string `json:"bill_to_pay"`
	BillToRefund         string `json:"bill_to_refund"`
	MerchantName         string `json:"merchant_name"`
	Street               string `json:"street"`
	Number               string `json:"number"`
	PostalCode           string `json:"postal_code"`
	Category             string `json:"category"`
	Channel              string `json:"channel"`
	GeographicCode       string `json:"geographic_code"`
	City                 string `json:"city"`
	MerchantID           string `json:"merchant_id"`
	Province             string `json:"province"`
	Country              string `json:"country"`
	MerchantEmail        string `json:"merchant_email"`
	MerchantPhone        string `json:"merchant_phone"`
}

type SubPayment struct {
	ID     string `json:"id"`
	Amount int64  `json:"amount"`
}

type Payment struct {
	ID                int64         `json:"id"`
	Status            *string       `json:"status"`
	StatusDetails     *StatusDetail `json:"status_details"`
	SiteTransactionID string        `json:"site_transaction_id"`
	Token             string        `json:"token"`
	PaymentMethodID   int64         `json:"payment_method_id"`
	Bin               string        `json:"bin"`
	Amount            int64         `json:"amount"`
	Currency          string        `json:"currency"`
	Installments      int64         `json:"installments"`
	Description       string        `json:"description"`
	EstablishmentName string        `json:"establishment_name"`
	PaymentType       string        `json:"payment_type"`
	Customer          Customer      `json:"customer"`
	AggregateData     AggregateData `json:"aggregate_data"`
	SubPayments       []SubPayment  `json:"sub_payments"`
}

type ValidationErrors struct {
	Code  string `json:"code,omitempty"`
	Param string `json:"param,omitempty"`
}

type PaymentError struct {
	ID               string             `json:"id,omitempty"`
	ErrorType        string             `json:"error_type,omitempty"`
	EntityName       string             `json:"entity_name,omitempty"`
	Message          string             `json:"message,omitempty"`
	ValidationErrors []ValidationErrors `json:"validation_errors,omitempty"`
}
