package decidir

import "fmt"

const (
	PaymentMethodKeyPattern = "%s_%s"

	Visa       = "visa"
	Mastercard = "mastercard"
	Cabal      = "cabal"
	Naranja    = "naranja"
	Amex       = "amex"

	Credit = "credit"
	Debit  = "debit"

	ApprovedStatus = "approved"
	RejectedStatus = "rejected"

	VisaCreditPaymentMethodValue       = 1
	VisaDebitPaymentMethodValue        = 31
	MastercardCreditPaymentMethodValue = 104
	MasterCardDebitPaymentMethodValue  = 105
	CabalCreditPaymentMethodValue      = 63
	CabalDebitPaymentMethodValue       = 108
	NaranjaCreditPaymentMethodValue    = 24
	AmexCreditPaymentMethodValue       = 65
)

var (
	validPaymentMethods = []string{
		Visa,
		Mastercard,
		Cabal,
		Naranja,
		Amex,
	}

	validPaymentMethodTypes = []string{
		Credit,
		Debit,
	}

	VisaCreditPaymentMethodKey       = fmt.Sprintf(PaymentMethodKeyPattern, Visa, Credit)
	VisaDebitPaymentMethodKey        = fmt.Sprintf(PaymentMethodKeyPattern, Visa, Debit)
	MastercardCreditPaymentMethodKey = fmt.Sprintf(PaymentMethodKeyPattern, Mastercard, Credit)
	MasterCardDebitPaymentMethodKey  = fmt.Sprintf(PaymentMethodKeyPattern, Mastercard, Debit)
	CabalCreditPaymentMethodKey      = fmt.Sprintf(PaymentMethodKeyPattern, Cabal, Credit)
	CabalDebitPaymentMethodKey       = fmt.Sprintf(PaymentMethodKeyPattern, Cabal, Debit)
	NaranjaCreditPaymentMethodKey    = fmt.Sprintf(PaymentMethodKeyPattern, Naranja, Credit)
	AmexCreditPaymentMethodKey       = fmt.Sprintf(PaymentMethodKeyPattern, Amex, Credit)

	paymentMethodMap = map[string]int64{
		VisaCreditPaymentMethodKey:       VisaCreditPaymentMethodValue,
		VisaDebitPaymentMethodKey:        VisaDebitPaymentMethodValue,
		MastercardCreditPaymentMethodKey: MastercardCreditPaymentMethodValue,
		MasterCardDebitPaymentMethodKey:  MasterCardDebitPaymentMethodValue,
		CabalCreditPaymentMethodKey:      CabalCreditPaymentMethodValue,
		CabalDebitPaymentMethodKey:       CabalDebitPaymentMethodValue,
		NaranjaCreditPaymentMethodKey:    NaranjaCreditPaymentMethodValue,
		AmexCreditPaymentMethodKey:       AmexCreditPaymentMethodValue,
	}
)
