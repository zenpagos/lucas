package decidir

import (
	"fmt"
	"github.com/go-resty/resty/v2"
	"github.com/sirupsen/logrus"
	"time"
)

const (
	timeOut = 300 * time.Second
	devURL  = "https://developers.decidir.com/api/v1"
	prodURL = "https://live.decidir.com/api/v1"

	devEnv  = "dev"
	prodEnv = "prod"

	// header keys
	apiKeyHeaderKey       = "apikey"
	cacheControlHeaderKey = "Cache-Control"
	contentTypeHeaderKey  = "Content-Type"

	// header values
	noCacheHeaderValue         = "no-cache"
	applicationJSONHeaderValue = "application/json"

	PaymentUri = "/payments"
)

type Client struct {
	log         *logrus.Logger
	restyClient *resty.Client
	privateKey  string
	environment string
	host        string
}

func NewDecidirClient(log *logrus.Logger) *Client {
	rc := resty.New().SetTimeout(timeOut)

	return &Client{
		log:         log,
		restyClient: rc,
	}
}

func (c *Client) SetCredentials(privateKey string) {
	c.privateKey = privateKey
}

func (c *Client) SetEnvironment(environment string) {
	c.host = devURL
	c.environment = environment
	c.restyClient.SetHostURL(c.host)

	if c.environment == prodEnv {
		c.host = prodURL
		c.restyClient.SetHostURL(c.host)
	}
}

// find takes a slice and looks for an element in it.
func (c Client) find(slice []string, val string) bool {
	for _, item := range slice {
		if item == val {
			return true
		}
	}
	return false
}

// GetPaymentMethodID receive a payment method and a payment method type
// and returns a Decidir Payment Method ID.
// Example:
//		GetPaymentMethodID("visa", "credit") returns -> 1
func (c Client) GetPaymentMethodID(pm string, pmt string) (int64, error) {
	if exists := c.find(validPaymentMethods, pm); exists == false {
		c.log.WithFields(logrus.Fields{
			"valid_payment_methods":   validPaymentMethods,
			"received_payment_method": pm,
		}).Error()
		return 0, ErrPaymentMethodNotValid
	}

	if exists := c.find(validPaymentMethodTypes, pmt); exists == false {
		c.log.WithFields(logrus.Fields{
			"valid_payment_method_types":   validPaymentMethodTypes,
			"received_payment_method_type": pmt,
		}).Error()
		return 0, ErrPaymentMethodTypeNotValid
	}

	k := fmt.Sprintf(PaymentMethodKeyPattern, pm, pmt)
	v, exists := paymentMethodMap[k]
	if !exists {
		c.log.WithFields(logrus.Fields{
			"payment_method_map": paymentMethodMap,
			"payment_method_key": k,
		}).Error()
		return 0, ErrPaymentMethodIDDoesNotExist
	}

	return v, nil
}

func (c Client) ExecutePayment(p *Payment) error {
	res, err := c.restyClient.R().
		SetHeader(apiKeyHeaderKey, c.privateKey).
		SetHeader(cacheControlHeaderKey, noCacheHeaderValue).
		SetHeader(contentTypeHeaderKey, applicationJSONHeaderValue).
		SetBody(p).
		SetResult(p).
		SetError(PaymentError{}).
		Post(PaymentUri)

	if err != nil {
		c.log.WithError(err).Error()
		return err
	}

	if res.IsError() {
		c.log.WithFields(logrus.Fields{
			"payment":     p,
			"url":         res.Request.URL,
			"status_code": res.StatusCode(),
			"received_at": res.ReceivedAt(),
			"response":    res.String(),
			"error":       res.Error(),
		}).Error("error creating the payment on Decidir")
		return ErrExecutingPayment
	}

	return nil
}
