package decidir

import "errors"

var (
	ErrGettingToken                = errors.New("error getting the token from Decidir")
	ErrExecutingPayment            = errors.New("error executing payment on Decidir")
	ErrPaymentMethodNotValid       = errors.New("payment method not valid")
	ErrPaymentMethodTypeNotValid   = errors.New("payment method type not valid")
	ErrPaymentMethodIDDoesNotExist = errors.New("payment method id does not exist")
)
