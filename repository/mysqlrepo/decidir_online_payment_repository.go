package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/lucas/model"
	"gitlab.com/zenpagos/lucas/repository"
)

type DecidirOnlinePaymentRepository struct {
	DB *gorm.DB
}

func NewDecidirOnlinePaymentRepository(db *gorm.DB) repository.DecidirOnlinePaymentRepositoryInterface {
	return DecidirOnlinePaymentRepository{
		DB: db,
	}
}

func (r DecidirOnlinePaymentRepository) Insert(o *model.DecidirOnlinePayment) error {
	return r.DB.Create(&o).Error
}

func (r DecidirOnlinePaymentRepository) Update(o *model.DecidirOnlinePayment) error {
	return r.DB.Save(&o).Error
}
