package container

import "gitlab.com/zenpagos/lucas/usecase"

type Container struct {
	DecidirOnlinePaymentUseCase usecase.DecidirOnlinePaymentUseCaseInterface
}

func NewContainer(
	dopuc usecase.DecidirOnlinePaymentUseCaseInterface,
) Container {
	return Container{
		DecidirOnlinePaymentUseCase: dopuc,
	}
}
