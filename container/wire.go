//+build wireinject

package container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/lucas/decidir"
	"gitlab.com/zenpagos/lucas/repository/mysqlrepo"
	"gitlab.com/zenpagos/lucas/usecase/deonpayuc"
)

func InitializeContainer(db *gorm.DB, log *logrus.Logger, dcd *decidir.Client) Container {
	wire.Build(
		// repositories
		mysqlrepo.NewDecidirOnlinePaymentRepository,
		// use Cases
		deonpayuc.NewDecidirOnlinePaymentUseCase,
		// container
		NewContainer,
	)

	return Container{}
}
